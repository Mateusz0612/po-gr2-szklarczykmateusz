package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

public class RachunekBankowyTest {

    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy(2000.00);
        RachunekBankowy saver2 = new RachunekBankowy(3000.00);
        RachunekBankowy.rocznaStopaProcentowa = 0.04;
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());
        System.out.println(RachunekBankowy.rocznaStopaProcentowa);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("Saver1 saldo po roku " + saver1.getSaldo());
        System.out.println("Saver2 saldo po roku " + saver2.getSaldo());
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        System.out.println(RachunekBankowy.rocznaStopaProcentowa);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("Saver1 saldo po kolejnym roku " + saver1.getSaldo());
        System.out.println("Saver2 saldo po kolejnym roku " + saver2.getSaldo());
    }
}
