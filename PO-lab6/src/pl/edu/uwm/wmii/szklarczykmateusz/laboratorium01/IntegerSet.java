package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

public class IntegerSet {

    public static void writeSet(boolean[] set) {
        for (int i = 0; i < 100; i++) {
            if (set[i]) System.out.println(i + 1);
        }
    }

    public static boolean[] union(boolean[] set1, boolean[] set2) {
        boolean[] sum = new boolean[100];
        for (int i = 0; i < 100; i++) {
            if (!set1[i] && !set2[i]) sum[i] = false;
            else sum[i] = true;
        }
        return sum;
    }

    public static boolean[] intersection(boolean[] set1, boolean[] set2) {
        boolean[] sum = new boolean[100];
        for (int i = 0; i < 100; i++) {
            if (set1[i] && set2[i]) sum[i] = true;
            else sum[i] = false;
        }
        return sum;
    }

    public void insertElement(int value) {
        if (value < 1 || value > 100) {
            System.out.println("Mozesz dodac tylko liczby z przedzialu 1-100");
        } else set[value - 1] = true;
    }

    public void deleteElement(int value) {
        set[value - 1] = false;
    }

    public String toString() {
        StringBuffer returnString = new StringBuffer();
        for (int i = 0; i < 100; i++) {
            if (set[i]) {
                returnString.append(i + 1);
                returnString.append(' ');
            }
        }
        return returnString.toString();
    }

    public boolean equals(Object otherObject){
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        IntegerSet other = (IntegerSet) otherObject;
        for(int i=0; i<100; i++){
            if(set[i] != other.set[i]) return false;
        }
        return true;
    }

    boolean[] set = new boolean[100];
}
