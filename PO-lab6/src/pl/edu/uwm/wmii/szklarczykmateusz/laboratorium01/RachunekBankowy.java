package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

public class RachunekBankowy {

    static double rocznaStopaProcentowa;
    private double saldo;

    public RachunekBankowy(double saldo){
        this.saldo = saldo;
    }

    public void setSaldo(double value) {
        saldo = value;
    }

    public double getSaldo() {
        return saldo;
    }

    public static void setRocznaStopaProcentowa(double value) {
        rocznaStopaProcentowa = value;
    }

    public void obliczMiesieczneOdsetki(){
        double saldo = this.getSaldo();
        double odsetki = (saldo * rocznaStopaProcentowa)/ 12;
        this.setSaldo(saldo += odsetki);
    }

}
