package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

public class IntegerSetTest {

    public static void main(String[] args) {
        IntegerSet set1 = new IntegerSet();
        IntegerSet set2 = new IntegerSet();
        for(int i=0; i<100; i++){
            set1.set[i] = (i + 1) % 2 == 0;
        }
        for(int i=0; i<100; i++){
            set2.set[i] = (i + 1) % 3 == 0;
        }
//        IntegerSet.writeSet(set1.set);
//        IntegerSet.writeSet(set2.set);
        boolean[] unioned = IntegerSet.union(set1.set, set2.set);
//        IntegerSet.writeSet(unioned);
        boolean[] intersectioned = IntegerSet.intersection(set1.set, set2.set);
//        IntegerSet.writeSet(intersectioned);
        set1.insertElement(3);
//        IntegerSet.writeSet(set1.set);
        System.out.println(set1);
        set1.deleteElement(2);
        System.out.println(set2);
        System.out.println(set1.equals(set2));
    }
}
