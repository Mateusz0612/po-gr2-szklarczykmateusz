package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.time.LocalDate;
import java.util.GregorianCalendar;

class Pracownik {
    private String nazwisko;
    private double pobory;
    private LocalDate dataZatrudnienia;

    public Pracownik(String var1, double var2, int var4, int var5, int var6) {
        this.nazwisko = var1;
        this.pobory = var2;
        GregorianCalendar var7 = new GregorianCalendar(var4, var5 - 1, var6);
        this.dataZatrudnienia = LocalDate.of(var4, var5, var6);
    }

    public String getNazwisko() {
        return this.nazwisko;
    }

    public double getPobory() {
        return this.pobory;
    }

    public LocalDate getDataZatrudnienia() {
        return LocalDate.of(this.dataZatrudnienia.getYear(), this.dataZatrudnienia.getMonthValue(), this.dataZatrudnienia.getDayOfMonth());
    }

    public void zwiekszPobory(double var1) {
        double var3 = this.pobory * var1 / 100.0D;
        this.pobory += var3;
    }

    public boolean equals(Object var1) {
        if (this == var1) {
            return true;
        } else if (var1 == null) {
            return false;
        } else if (this.getClass() != var1.getClass()) {
            return false;
        } else {
            Pracownik var2 = (Pracownik)var1;
            return this.nazwisko.equals(var2.nazwisko) && this.pobory == var2.pobory && this.dataZatrudnienia.equals(var2.dataZatrudnienia);
        }
    }

    public int hashCode() {
        return 7 * this.nazwisko.hashCode() + 11 * (new Double(this.pobory)).hashCode() + 13 * this.dataZatrudnienia.hashCode();
    }

    public String toString() {
        LocalDate var1 = this.getDataZatrudnienia();
        return this.getClass().getName() + "[nazwisko=" + this.nazwisko + ",pobory=" + this.pobory + ",dataZatrudnienia=" + var1 + "]";
    }
}
