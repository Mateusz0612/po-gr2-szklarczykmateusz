package pl.edu.uwm.wmii.szklarczykmateusz.lab13;

public class Task implements Comparable<Task> {
    private int priority;
    private String description;

    public Task(int priority, String description) {
        this.priority = priority;
        this.description = description;
    }

    public int getPriority() {
        return priority;
    }

    public String getDescription() {
        return description;
    }

    public int compareTo(Task o) {
        if (priority > o.priority) return 1;
        else if (priority < o.priority) return -1;
        return 0;
    }
}
