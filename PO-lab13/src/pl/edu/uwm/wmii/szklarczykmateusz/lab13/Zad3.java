package pl.edu.uwm.wmii.szklarczykmateusz.lab13;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Zad3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        TreeMap<Student, String> gradeList = new TreeMap<>();
        TreeMap<Integer, Student> idList = new TreeMap<>();
        int choose;
        do {
            System.out.println("1. Add student\n2. Remove student\n3. Change student grade\n4. Show student grades");
            choose = scan.nextInt();
            switch (choose) {
                case 0:
                    System.out.println("Ending program");
                    break;
                case 1:
                    System.out.println("Enter name");
                    String name = scan.next();
                    System.out.println("Enter second name");
                    String secondName = scan.next();
                    Student newStudent = new Student(name, secondName);
                    gradeList.put(newStudent, "");
                    idList.put(newStudent.getId(), newStudent);
                    break;
                case 2:
                    System.out.println("Enter student ID");
                    int id = scan.nextInt();
                    Student choosedStudent = idList.get(id);
                    gradeList.remove(choosedStudent);
                    System.out.println("Student was successfully removed");
                    break;
                case 3:
                    System.out.println("Enter student ID");
                    id = scan.nextInt();
                    choosedStudent = idList.get(id);
                    System.out.println("Enter new grade");
                    String newGrade = scan.next();
                    gradeList.replace(choosedStudent, newGrade);
                    break;
                case 4:
                    for (Map.Entry<Student, String> entry : gradeList.entrySet()) {
                        Student key = entry.getKey();
                        String value = entry.getValue();
                        System.out.println(key + " => " + value);
                    }
                    break;
                default:
                    System.out.println("Incorrect operation");
                    break;
            }
        } while (choose != 0);

    }
}
