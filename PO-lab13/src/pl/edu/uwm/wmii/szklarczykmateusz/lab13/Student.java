package pl.edu.uwm.wmii.szklarczykmateusz.lab13;

public class Student implements Comparable<Student> {
    private String imie;
    private String nazwisko;
    private int id;
    static int counter = 0;

    public Student(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.id = counter;
        counter++;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getId() {
        return id;
    }


    @Override
    public int compareTo(Student o) {
        int result = nazwisko.compareTo(o.nazwisko);
        if (result == 0) {
            int result2 = imie.compareTo(o.imie);
            if (result2 == 0) {
                return Integer.compare(id, o.id);
            } else return imie.compareTo(o.imie);
        } else return nazwisko.compareTo(o.nazwisko);
    }

    @Override
    public String toString() {
        return nazwisko + " " + imie + " " + id + " ";
    }
}
