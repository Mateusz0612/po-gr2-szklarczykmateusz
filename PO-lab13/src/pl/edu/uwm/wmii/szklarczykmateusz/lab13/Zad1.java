package pl.edu.uwm.wmii.szklarczykmateusz.lab13;

import java.util.Scanner;

public class Zad1 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String operation;
        ToDoList tasks = new ToDoList();
        do {
            System.out.println("1.Wpisz 'dodaj' aby dodac zadanie.\n" +
                    "2.Wpisz 'nastepne' aby usunac zadanie z najwiekszym priorytetem.\n" +
                    "3.Wpisz 'wyswietl' aby wyswietlic zadania\n" +
                    "4.Wpisz 'zakoncz' aby wyjsc.\n"
            );
            operation = scan.next();
            switch (operation) {
                case "dodaj":
                    System.out.println("Podaj priorytet zadania");
                    int taskPriority = scan.nextInt();
                    System.out.println("Podaj opis zadania");
                    String taskDescription = scan.next();
                    taskDescription += scan.nextLine();
                    Task task = new Task(taskPriority, taskDescription);
                    tasks.addTask(task);
                    break;
                case "nastepne":
                    tasks.deleteTask();
                    break;
                case "wyswietl":
                    System.out.println("Zdania na liscie:");
                    tasks.showTasks();
                    break;
                default:
                    if (operation.compareTo("zakoncz") == 0) break;
                    System.out.println("Przykro mi ale nie ma takiej operacji");
                    break;
            }
        } while (operation.compareTo("zakoncz") != 0);
    }
}
