package pl.edu.uwm.wmii.szklarczykmateusz.lab13;

import java.util.PriorityQueue;

public class ToDoList {
    private PriorityQueue<Task> list;

    public ToDoList() {
        this.list = new PriorityQueue<Task>();
    }

    public void addTask(Task t) {
        list.add(t);
    }

    public void deleteTask() {
        list.remove();
    }

    public void showTasks() {
        if (list.size() == 0) System.out.println("Brak zadan na liscie");
        else {
            for (Task t : list)
                System.out.print("Task description: " + t.getDescription() + " Priority: " + t.getPriority() + " | ");
            System.out.println("\n");
        }
    }
}
