package pl.edu.uwm.wmii.szklarczykmateusz.lab13;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Zad4 {
    public static void main(String[] args) {
        Map<Integer, HashSet<String>> items = new TreeMap<>();
        try {
            File file = new File("alice30.txt");
            Scanner scan = new Scanner(file);
            while (scan.hasNext()) {
                String string = scan.next();
                if (!items.containsKey(string.length())) {
                    HashSet<String> words = new HashSet<>();
                    words.add(string);
                    items.put(string.length(), words);
                } else {
                    HashSet<String> words = items.get(string.length());
                    words.add(string);
                    items.replace(string.length(), words);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        for (Map.Entry<Integer, HashSet<String>> entry : items.entrySet()) {
            Integer key = entry.getKey();
            HashSet<String> values = entry.getValue();
            if (values.size() > 1) {
                System.out.println(key);
                for (String element : values) System.out.print(element + ",");
                System.out.println("\n");
            }
        }
    }
}
