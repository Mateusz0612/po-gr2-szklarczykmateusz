package pl.edu.uwm.wmii.szklarczykmateusz.lab13;

import java.util.Map;
import java.util.TreeMap;

public class StudentGrades {
    private TreeMap<String, String> gradesList;

    public StudentGrades(){
        this.gradesList = new TreeMap<>();
    }

    public void addStudent(String name){
        gradesList.put(name, "");
    }

    public void removeStudent(String name){
        gradesList.remove(name);
    }

    public void changeGrade(String name, String newGrade){
        gradesList.replace(name, gradesList.get(name), newGrade);
    }

    public void printStudentGrades(){
        for(Map.Entry<String, String> entry: gradesList.entrySet()){
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println(key + ":" + value);
        }
    }
}
