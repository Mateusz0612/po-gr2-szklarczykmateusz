package pl.edu.uwm.wmii.szklarczykmateusz.lab13;


import java.util.Scanner;

public class Zad2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int choose;
        StudentGrades studentList = new StudentGrades();
        String name;
        do {
            System.out.println("1. Add student\n2. Remove student\n3. Change student grade\n4. Show student grades");
            choose = scan.nextInt();
            switch (choose) {
                case 0:
                    break;
                case 1:
                    System.out.println("Enter student name");
                    name = scan.next();
                    studentList.addStudent(name);
                    break;
                case 2:
                    System.out.println("Enter student name");
                    name = scan.next();
                    studentList.removeStudent(name);
                    break;
                case 3:
                    System.out.println("Enter student name");
                    name = scan.next();
                    System.out.println("Enter new grade");
                    String newGrade = scan.next();
                    studentList.changeGrade(name, newGrade);
                    break;
                case 4:
                    System.out.println("Students grade");
                    studentList.printStudentGrades();
                    break;
                default:
                    System.out.println("Incorrect operation");
                    break;
            }
        } while (choose != 0);
    }

}
