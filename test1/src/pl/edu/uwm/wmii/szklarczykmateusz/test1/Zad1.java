package pl.edu.uwm.wmii.szklarczykmateusz.test1;

import java.util.Scanner;

public class Zad1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Give n number");
        int n = scan.nextInt();
        for (int i = n; i != n + n; i++) {
            if (isPrime(i)) System.out.println(i);
        }
    }

    public static boolean isPrime(int value) {
        for (int i = 2; i < value; i++) {
            if (value % i == 0) return false;
        }
        return true;
    }
}
