package pl.edu.uwm.wmii.szklarczykmateusz.test1;

import java.util.ArrayList;

public class Zad2 {
    public static void main(String[] args) {
        ArrayList<Integer> list1 = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();
        for (int i = 0; i < 5; i++) list1.add(i);
        for (int i = 5; i < 10; i++) list2.add(i);
        ArrayList<Integer> list3 = merge(list1, list2);
        for (int e : list3) System.out.println(e);
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> c = new ArrayList<>();
        int size = Math.max(a.size(), b.size());
        int aIndex = 0;
        int bIndex = 0;
        for (int i = 0; i < size; i++) {
            if((bIndex - 1) == b.size()){
                c.addAll(i, a);
                break;
            }
            else if((aIndex - 1) == a.size()){
                c.addAll(i, b);
                break;
            }
            c.add(a.get(aIndex));
            aIndex++;
            c.add(b.get(bIndex));
            bIndex++;
        }
        return c;
    }

}
