package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium05;

import java.util.ArrayList;

public class Zad5 {
    public static void main(String[] args) {
        ArrayList<Integer> arr1 = new ArrayList<>();
        arr1.add(1);
        arr1.add(2);
        arr1.add(3);
        arr1.add(4);
        reverse(arr1);
        for(int x: arr1) System.out.println(x);
    }

    public static void reverse(ArrayList<Integer> a) {
        ArrayList<Integer> newArr = new ArrayList<>();
        int index = 0;
        for (int i = a.size() - 1; i >= 0; i--) newArr.add(a.get(i));
        while (index != a.size()){
            a.set(index, newArr.get(index));
            index++;
        }
    }
}
