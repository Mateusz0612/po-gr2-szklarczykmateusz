package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium05;

import java.util.ArrayList;

public class Zad1 {
    public static void main(String[] args) {
        ArrayList<Integer> arr1 = new ArrayList<>();
        ArrayList<Integer> arr2 = new ArrayList<>();
        for(int i=1; i<=10; i++){
            arr1.add(i);
            arr2.add(i*2);
        }
        ArrayList<Integer> arr3 = append(arr1, arr2);
        for(int x: arr3) System.out.println(x);
    }

    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> newArr = new ArrayList<>();
        newArr.addAll(0, a);
        newArr.addAll(a.size(), b);
        return newArr;
    }
}
