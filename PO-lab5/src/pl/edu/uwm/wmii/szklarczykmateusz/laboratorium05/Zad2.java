package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium05;

import java.util.ArrayList;

public class Zad2 {
    public static void main(String[] args) {
        ArrayList<Integer> arr1 = new ArrayList<>();
        ArrayList<Integer> arr2 = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            arr1.add(i);
            arr2.add(i*2);
        }
        arr1.add(1);
        arr1.add(1);
        arr1.add(1);
        arr2.add(2);
        ArrayList<Integer> arr3 = merge(arr1, arr2);
        for (int x : arr3) System.out.println(x);
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> newArr = new ArrayList<>();
        int minSize = Math.min(a.size(), b.size());
        for (int i = 0; i < minSize; i++) {
            newArr.add(a.get(i));
            newArr.add(b.get(i));
        }
        if (a.size() > minSize) {
            for (int i = minSize; i < a.size(); i++){
                newArr.add(a.get(i));
            }
        }
        else if(b.size() > minSize){
            for (int i = minSize; i < b.size(); i++){
                newArr.add(b.get(i));
            }
        }
        return newArr;
    }
}
