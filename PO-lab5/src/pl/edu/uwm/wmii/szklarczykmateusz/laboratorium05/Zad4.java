package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium05;

import java.util.ArrayList;

public class Zad4 {
    public static void main(String[] args) {
        ArrayList<Integer> arr1 = new ArrayList<>();
        for(int i=1; i<=10; i++) arr1.add(i);
        ArrayList<Integer> reversedArr1 = reversed(arr1);
        for(int x: reversedArr1) System.out.println(x);
    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a) {
        ArrayList<Integer> newArr = new ArrayList<>();
        for(int i=a.size()-1; i>=0; i--){
            newArr.add(a.get(i));
        }
        return newArr;
    }
}
