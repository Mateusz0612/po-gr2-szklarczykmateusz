package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium05;

import java.util.ArrayList;
import java.util.Collections;

public class Zad3 {
    public static void main(String[] args) {
        ArrayList<Integer> arr1 = new ArrayList<>();
        ArrayList<Integer> arr2 = new ArrayList<>();
        //arr1
        arr1.add(1);
        arr1.add(4);
        arr1.add(9);
        arr1.add(16);
        //arr2
        arr2.add(9);
        arr2.add(7);
        arr2.add(4);
        arr2.add(9);
        arr2.add(11);
        ArrayList<Integer> arr3 = mergeSorted(arr1, arr2);
        for (int x : arr3) System.out.println(x);
    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> newArr = new ArrayList<>();
        Collections.sort(a);
        Collections.sort(b);
        int index = 0;
        while (!a.isEmpty() && !b.isEmpty()) {
            if (b.get(index) >= a.get(index)) {
                newArr.add(a.get(index));
                a.remove(index);
            } else if (b.get(index) < a.get(index)) {
                newArr.add(b.get(index));
                b.remove(index);
            }
        }
        if (!a.isEmpty()) newArr.addAll(newArr.size(), a);
        if (!b.isEmpty()) newArr.addAll(newArr.size(), b);
        return newArr;
    }

}
