package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium00;

public class Zadanie10 {
    public static void main(String[] args) {
        String[] movies = {"Wolf of Wall Street", "Hangover", "Once upon a time in Hollywood"};
        for (String movie : movies) {
            System.out.println(movie);
        }
    }
}
