package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium00;

public class Zadanie11 {
    public static void main(String[] args) {
        String poem = "To ballada o zabitej,\n" +
                "która nagle z krzesła wstała.\n" +
                "Ułożona w dobrej wierze,\n" +
                "napisana na papierze.\n" +
                "Przy nie zasłoniętym oknie,\n" +
                "w świetle lampy rzecz się miała.\n" +
                "Każdy, kto chciał, widzieć mógł.\n" +
                "Kiedy się zamknęły drzwi\n" +
                "i zabójca zbiegł ze schodów,\n" +
                "ona wstała tak jak żywi\n" +
                "nagłą ciszą obudzeni.\n" +
                "Ona wstała, rusza głową\n" +
                "i twardymi jak z pierścionka\n" +
                "oczami patrzy po kątach.\n" +
                "Nie unosi się w powietrzu,\n" +
                "ale po zwykłej podłodze,\n" +
                "po skrzypiących deskach stąpa.\n" +
                "Wszystkie po zabójcy ślady\n" +
                "pali w piecu. Aż do szczętu\n" +
                "fotografii, do imentu\n" +
                "sznurowadła z dna szuflady.\n" +
                "Ona nie jest uduszona.\n" +
                "Ona nie jest zastrzelona.\n" +
                "Niewidoczną śmierć poniosła.\n" +
                "Może dawać znaki życia,\n" +
                "płakać z różnych drobnych przyczyn,\n" +
                "nawet krzyczeć z przerażenia\n" +
                "na widok myszy.\n" +
                "Tak wiele\n" +
                "jest słabości i śmieszności\n" +
                "nietrudnych do podrobienia.\n" +
                "Ona wstała, jak się wstaje.\n" +
                "Ona chodzi, jak się chodzi.\n" +
                "Nawet śpiewa czesząc włosy,\n" +
                "które rosną.";
        System.out.println(poem);
    }
}
