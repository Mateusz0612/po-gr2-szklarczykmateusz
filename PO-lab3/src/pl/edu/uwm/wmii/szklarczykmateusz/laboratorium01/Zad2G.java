package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad2G {

    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n (0-100)");
        int n = scan.nextInt();
        if (n < 0 || n > 100) {
            System.out.println("Liczba musi byc z przedzialu 0-100");
            return;
        }
        int[] arr = new int[n];
        Zad2A.generate(arr, n, -999, 999);
        System.out.println("-----");
        odwrocFragment(arr, 0, 5);
    }

    public static void odwrocFragment(int[] tab, int left, int right){
        int[] notReversed = new int[right-left+1];
        int[] reversed = new int[right-left+1];
        int k = 0;
        for(int i=left; i<=right; i++){
            notReversed[k] = tab[i];
            k++;
        }
        k = 0;
        for(int i=notReversed.length-1; i>=0; i--){
            reversed[k] = notReversed[i];
            k++;
        }
        k = 0;
        for(int i=left; i<=right; i++){
            tab[i] = reversed[k];
            k++;
        }
        for(int element: tab){
            System.out.println(element);
        }
    }

}
