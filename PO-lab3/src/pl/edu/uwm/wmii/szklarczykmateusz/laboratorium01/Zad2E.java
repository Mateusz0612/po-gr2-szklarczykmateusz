package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad2E {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n (0-100)");
        int n = scan.nextInt();
        if (n < 0 || n > 100) {
            System.out.println("Liczba musi byc z przedzialu 0-100");
            return;
        }
        int[] arr = new int[n];
        Zad2A.generate(arr, n, -999, 999);
        int m = dlugoscMaksymalnegoCiaguDodatnich(arr);
        System.out.println("Najdluzszy ciag dodatnich " + m);
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int[] array) {
        int n = array.length;
        int[] longest = new int[n];
        int counter = 1;
        for (int i = 0; i < n - 1; i++) {
            if (array[i] > 0 && array[i + 1] > 0) {
                counter++;
            }
            else if (array[i + 1] <= 0) {
                longest[i] = counter;
                counter = 1;
            }
            longest[i] = counter;
        }
        int max = longest[0];
        for (int e : longest) {
            if (e > max) max = e;
        }
        return max;
    }
}
