package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad3 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe m,n,k(1-10)");
        int m = scan.nextInt();
        if (m < 1 || m > 10) {
            System.out.println("M musi byc z zakresu 1-10");
            return;
        }
        int n = scan.nextInt();
        if (n < 1 || n > 10) {
            System.out.println("N musi byc z zakresu 1-10");
            return;
        }
        int k = scan.nextInt();
        if (k < 1 || k > 10) {
            System.out.println("K musi byc z zakresu 1-10");
            return;
        }

        int[][] a = new int[m][n];
        int[][] b = new int[n][k];

        generujMacierz(a, m, n);
        generujMacierz(b, n, k);

        int[][] c = pomnozMacierze(a, b);

        System.out.println("Macierz A");
        wypiszMacierz(a, m, n);
        System.out.println("Macierz B");
        wypiszMacierz(b, n, k);
        System.out.println("Macierz C");
        wypiszMacierz(c, c.length, c[0].length);
    }

    public static void generujMacierz(int[][] arr, int a, int b) {
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                arr[i][j] = Zad2A.generateNumber(1, 10);
            }
        }
    }

    public static void wypiszMacierz(int[][] arr, int a, int b) {
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.print('\n');
        }
    }

    public static int[][] pomnozMacierze(int[][] a, int[][] b) {
        int[][] c = new int[a.length][b[0].length];
        if (a[0].length == b.length) {
            for (int i = 0; i < c.length; i++) {
                for (int j = 0; j < c[i].length; j++) {
                    for (int k = 0; k < a[0].length; k++) {
                        c[i][j] += a[i][k] * b[k][j];
                    }
                }
            }
        }
        else{
            System.out.println("Nie mozna pomnozyc takiej macierzy");
        }
        return c;
    }

}
