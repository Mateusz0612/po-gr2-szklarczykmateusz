package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad1F {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj długość tablicy (1-100)");
        int n = scan.nextInt();
        if (n < 1 || n > 100) {
            System.out.println("Dlugosc tablicy musi byc w zakresie 1-100");
            return;
        }
        int[] array = new int[n];
        for(int i=0; i<n; i++){
            System.out.println("Podaj liczbe calkowita");
            array[i] = scan.nextInt();
        }
        for(int e: array){
            if(e > 0) e = 1;
            else if (e < 0 ) e = -1;
            System.out.println(e);
        }
    }
}
