package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;
import java.util.Random;

public class Zad1A {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj długość tablicy (1-100)");
        int oddNumbers = 0;
        int evenNumbers = 0;
        int n = scan.nextInt();
        if (n < 1 || n > 100) {
            System.out.println("Dlugosc tablicy musi byc w zakresie 1-100");
            return;
        }
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe calkowita");
            array[i] = scan.nextInt();
            if (array[i] % 2 == 0) evenNumbers++;
            else oddNumbers++;
        }
        System.out.println("W podanej tablicy znajduje sie " + evenNumbers + " parzysta/ych, oraz " + oddNumbers + " nieparzysta/ych");
    }
}
