package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad2C {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n (0-100)");
        int n = scan.nextInt();
        if (n < 0 || n > 100) {
            System.out.println("Liczba musi byc z przedzialu 0-100");
            return;
        }
        int[] arr = new int[n];
        Zad2A.generate(arr, n, -999, 999);
        int maxElements = ileMaksymalnych(arr);
        System.out.println("Elementow maks " + maxElements);
    }

    public static int ileMaksymalnych(int[] tab) {
        int maxElement = tab[0];
        int counter = 0;
        for (int i = 0; i < tab.length; i++) if (tab[i] > maxElement) maxElement = tab[i];
        for (int element : tab) {
            if (element == maxElement) counter++;
        }
        return counter;
    }
}
