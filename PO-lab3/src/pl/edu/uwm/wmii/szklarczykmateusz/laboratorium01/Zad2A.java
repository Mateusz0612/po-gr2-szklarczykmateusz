package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Random;
import java.util.Scanner;

public class Zad2A {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe calkowita (0,100):");
        int n = scan.nextInt();
        if (n < 0 || n > 100) {
            System.out.println("Liczba musi byc z przedzialu 0-100");
            return;
        }
        int[] arr = new int[n];
        generate(arr, n, -999, 999);
        int odd = ileNieparzystych(arr);
        int even = ileParzystych(arr);
        System.out.println("Nieparzyste: " + odd + " Parzyste: " + even);
    }

    public static int generateNumber(int min, int max) {
        Random rand = new Random();
        return rand.nextInt(max - min) + min;
    }

    public static void generate(int[] tab, int n, int min, int max) {
        for (int i = 0; i < n; i++) {
            tab[i] = generateNumber(min, max);
            System.out.println(tab[i]);
        }
    }

    public static int ileNieparzystych(int[] tab) {
        int counter = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] % 2 != 0) counter++;
        }
        return counter;
    }

    public static int ileParzystych(int[] tab) {
        int counter = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] % 2 == 0) counter++;
        }
        return counter;
    }
}
