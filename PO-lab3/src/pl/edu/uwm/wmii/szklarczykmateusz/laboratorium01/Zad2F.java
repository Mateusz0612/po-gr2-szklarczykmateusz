package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad2F {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n (0-100)");
        int n = scan.nextInt();
        if (n < 0 || n > 100) {
            System.out.println("Liczba musi byc z przedzialu 0-100");
            return;
        }
        int[] arr = new int[n];
        Zad2A.generate(arr, n, -999, 999);
        System.out.println("-------");
        signum(arr);
    }

    public static void signum(int[] tab){
        for(int i=0; i<tab.length; i++){
            if(tab[i] > 0) tab[i] = 1;
            if(tab[i] < 0) tab[i] = -1;
            System.out.println(tab[i]);
        }
    }
}
