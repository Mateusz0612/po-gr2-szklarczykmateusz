package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Arrays;
import java.util.Scanner;

public class Zad1E {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj długość tablicy (1-100)");
        int n = scan.nextInt();
        if (n < 1 || n > 100) {
            System.out.println("Dlugosc tablicy musi byc w zakresie 1-100");
            return;
        }
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe calkowita");
            array[i] = scan.nextInt();
        }
        int[] longest = new int[n];
        int counter = 1;
        for (int i = 0; i < n - 1; i++) {
            if (array[i] > 0 && array[i + 1] > 0) {
                counter++;
            }
            else if (array[i + 1] <= 0) {
                longest[i] = counter;
                counter = 1;
            }
            longest[i] = counter;
        }
        int max = longest[0];
        for (int e : longest) {
            if (e > max) max = e;
        }
        System.out.println(max);
    }
}

