package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad2D {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n (0-100)");
        int n = scan.nextInt();
        if (n < 0 || n > 100) {
            System.out.println("Liczba musi byc z przedzialu 0-100");
            return;
        }
        int[] arr = new int[n];
        Zad2A.generate(arr, n, -999, 999);
        int sumOfPositiveNumbers = sumaDodatnich(arr);
        int sumOfNegativeNumbers = sumaUjemnych(arr);
        System.out.println("Suma dodatnich: " + sumOfPositiveNumbers + " Suma ujemnych: " + sumOfNegativeNumbers);
    }

    public static int sumaDodatnich(int[] tab) {
        int result = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] > 0) result += tab[i];
        }
        return result;
    }

    public static int sumaUjemnych(int[] tab) {
        int result = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] < 0) result += tab[i];
        }
        return result;
    }
}
