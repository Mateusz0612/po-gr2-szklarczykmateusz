package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad1C {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj długość tablicy (1-100)");
        int maxElement;
        int counter = 0;
        int n = scan.nextInt();
        if (n < 1 || n > 100) {
            System.out.println("Dlugosc tablicy musi byc w zakresie 1-100");
            return;
        }
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe calkowita");
            array[i] = scan.nextInt();
        }
        maxElement = array[0];
        for (int i = 0; i < n; i++) {
            if (array[i] > maxElement) maxElement = array[i];
        }
        for (int element : array) {
            if (element == maxElement) counter++;
        }
        System.out.println("Najwiekszy element to: " + maxElement + " wystapil: " + counter);
    }
}
