package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad1G {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj długość tablicy (1-100)");
        int n = scan.nextInt();
        if (n < 1 || n > 100) {
            System.out.println("Dlugosc tablicy musi byc w zakresie 1-100");
            return;
        }
        int[] array = new int[n];
        for(int i=0; i<n; i++){
            System.out.println("Podaj liczbe calkowita");
            array[i] = scan.nextInt();
        }
        System.out.println("Podaj lewy");
        int left = scan.nextInt();
        System.out.println("Podaj prawy");
        int right = scan.nextInt();
        int[] notReversed = new int[right-left+1];
        int[] reversed = new int[right-left+1];
        int k = 0;
        for(int i=left; i<=right; i++){
            notReversed[k] = array[i];
            k++;
        }
        k = 0;
        for(int i=notReversed.length-1; i>=0; i--){
            reversed[k] = notReversed[i];
            k++;
        }
        k = 0;
        for(int i=left; i<=right; i++){
            array[i] = reversed[k];
            k++;
        }
        for(int element: array){
            System.out.println(element);
        }
    }
}
