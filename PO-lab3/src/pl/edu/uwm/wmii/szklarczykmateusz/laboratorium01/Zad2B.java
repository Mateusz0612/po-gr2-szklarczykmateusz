package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad2B {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n (0-100)");
        int n = scan.nextInt();
        if (n < 0 || n > 100) {
            System.out.println("Liczba musi byc z przedzialu 0-100");
            return;
        }
        int[] arr = new int[n];
        Zad2A.generate(arr, n, -999, 999);
        int positiveNumbers = ileDodatnich(arr);
        int negativeNumbers = ileUjemnych(arr);
        int zeros = ileZerowych(arr);
        System.out.println("Dodatnich: " + positiveNumbers + " Ujemnych:" + negativeNumbers + " Zerowych:" + zeros);
    }

    public static int ileDodatnich(int[] tab) {
        int counter = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] > 0) counter++;
        }
        return counter;
    }

    public static int ileUjemnych(int[] tab) {
        int counter = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] < 0) counter++;
        }
        return counter;
    }

    public static int ileZerowych(int[] tab) {
        int counter = 0;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == 0) counter++;
        }
        return counter;
    }
}
