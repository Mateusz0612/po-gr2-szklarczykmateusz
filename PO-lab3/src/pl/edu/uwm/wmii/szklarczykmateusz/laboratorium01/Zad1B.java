package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad1B {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj ilosc elementow tablicy");
        int positiveNumbers = 0;
        int negativeNumbers = 0;
        int zeros = 0;
        int n = scan.nextInt();
        if (n < 1 || n > 100) {
            System.out.println("Dlugosc tablicy musi byc w zakresie 1-100");
            return;
        }
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe calkowita");
            array[i] = scan.nextInt();
            if (array[i] > 0) positiveNumbers++;
            else if (array[i] < 0) negativeNumbers++;
            else zeros++;
        }
        System.out.println("Dodatnich: " + positiveNumbers + " Ujemnych: " + negativeNumbers + " Zera: " + zeros);
    }

}
