package pl.imiajd.szklarczyk;

import java.time.LocalDate;

public class Student extends Osoba implements Cloneable, Comparable<Osoba> {

    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public int compareTo(Student o){
        if(super.compareTo(o) == 0){
            if (sredniaOcen > o.sredniaOcen) return 1;
            else if (sredniaOcen < o.sredniaOcen) return -1;
            if (sredniaOcen == o.sredniaOcen) return 0;
        }
        return super.compareTo(o);
    }

    public String toString(){
        return super.toString() + " " + sredniaOcen;
    }
    private double sredniaOcen;
}
