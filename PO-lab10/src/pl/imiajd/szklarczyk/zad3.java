package pl.imiajd.szklarczyk;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class zad3 {
    public static void main(String[] args) {
        System.out.println(args[0]);
        if (args.length >= 1) {
            ArrayList<String> result = new ArrayList<>();
            String fileName = args[0];
            try {
                File f = new File(fileName);
                Scanner fileScanner = new Scanner(f);
                while (fileScanner.hasNextLine()) {
                    result.add(fileScanner.nextLine());
                }
            } catch (FileNotFoundException e) {
                System.out.println("File not found");
                e.printStackTrace();
            } finally {
                System.out.println("Before sorting");
                for (String e : result) System.out.println(e);
                System.out.println("After sorting");
                result.sort(String::compareTo);
                for (String e : result) System.out.println(e);
            }
        }
    }
}
