package pl.imiajd.szklarczyk;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestOsoba {
    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList<>();
        Osoba Marek = new Osoba("Kowalski", LocalDate.of(1997, 11, 12));
        Osoba Marek2 = new Osoba("Kowalski", LocalDate.of(1997, 3, 13));
        Osoba Ania1 = new Osoba("Piotrkowska", LocalDate.of(1997, 5, 3));
        Osoba Ania2 = new Osoba("Jerzynowska", LocalDate.of(1997, 5, 3));
        Osoba Ja = new Osoba("Szklarczyk", LocalDate.of(2000, 12, 6));
        try {
            Osoba jaV2 = (Osoba) Ja.clone();
            System.out.println(jaV2);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        grupa.add(Marek);
        grupa.add(Marek2);
        grupa.add(Ania1);
        grupa.add(Ania2);
        grupa.add(Ja);
        for (Osoba p : grupa) System.out.println(p);
        grupa.sort(Osoba::compareTo);
        System.out.println("Po sortowaniu");
        for (Osoba p : grupa) System.out.println(p);
    }
}
