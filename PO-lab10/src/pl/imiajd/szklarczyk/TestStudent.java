package pl.imiajd.szklarczyk;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestStudent {
    public static void main(String[] args){
        ArrayList<Student> grupa = new ArrayList<>();
        Student Marek = new Student("Kowalski", LocalDate.of(1997, 11, 12), 3.27);
        Student Marek2 = new Student("Kowalski", LocalDate.of(1997, 3, 13), 4.12);
        Student Ania1 = new Student("Piotrkowska", LocalDate.of(1997, 5, 3), 4.12);
        Student Ania2 = new Student("Jerzynowska", LocalDate.of(1997, 5, 3), 5.00);
        Student Ja = new Student("Szklarczyk", LocalDate.of(2000, 12, 6), 2.00);
        Student jaDwa = new Student("Szklarczyk", LocalDate.of(2000, 12, 6), 2.50);
        grupa.add(Marek);
        grupa.add(Marek2);
        grupa.add(Ania1);
        grupa.add(Ania2);
        grupa.add(Ja);
        grupa.add(jaDwa);
        for (Student p : grupa) System.out.println(p);
        grupa.sort(Student::compareTo);
        System.out.println("Po sortowaniu");
        for(Student p : grupa) System.out.println(p);
    }
}
