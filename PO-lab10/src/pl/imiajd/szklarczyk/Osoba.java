package pl.imiajd.szklarczyk;

import java.time.LocalDate;

public class Osoba implements Cloneable, Comparable<Osoba> {

    public Osoba(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return LocalDate.of(dataUrodzenia.getYear(), dataUrodzenia.getMonthValue(), dataUrodzenia.getDayOfMonth());
    }

    @Override
    public String toString() {
        return Osoba.class.getName() + "[" + nazwisko + " " + dataUrodzenia.getYear() + "-" + dataUrodzenia.getMonthValue() + "-" + dataUrodzenia.getDayOfMonth() + "]";
    }

    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        Osoba other = (Osoba) otherObject;
        return this.nazwisko.equals(other.nazwisko) && this.dataUrodzenia.equals(other.dataUrodzenia);
    }

    @Override
    public int compareTo(Osoba o) {
        int compareResult = this.nazwisko.compareTo(o.nazwisko);
        if (compareResult == 0) return this.dataUrodzenia.compareTo(o.dataUrodzenia);
        else {
            return compareResult;
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    private String nazwisko;
    private LocalDate dataUrodzenia;
}
