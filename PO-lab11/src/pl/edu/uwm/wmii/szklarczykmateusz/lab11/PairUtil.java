package pl.edu.uwm.wmii.szklarczykmateusz.lab11;

public class PairUtil<T> extends Pair<T> {
    public PairUtil(T first, T second) {
        super(first, second);
    }

    public static <T> Pair<T> swap(Pair<T> para) {
        para.swap();
        return new Pair<>(para.getFirst(), para.getSecond());
    }
}
