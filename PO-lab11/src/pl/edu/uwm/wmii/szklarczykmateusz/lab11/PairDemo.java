package pl.edu.uwm.wmii.szklarczykmateusz.lab11;

public class PairDemo {
    public static void main(String[] args) {
        Pair<Integer> para = new Pair<>(1, 2);
        System.out.println("Przed zmiana - " + para.getFirst() + " " + para.getSecond());
        para.swap();
        System.out.println("Po zmianie - " + para.getFirst() + " " + para.getSecond());
        System.out.println("Po zmianie - metoda statyczna");
        Pair<Integer> paraV1 = PairUtil.swap(para);
        System.out.println(paraV1.getFirst() + " " + paraV1.getSecond());
    }
}
