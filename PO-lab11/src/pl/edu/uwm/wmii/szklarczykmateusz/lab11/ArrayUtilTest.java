package pl.edu.uwm.wmii.szklarczykmateusz.lab11;


import java.time.LocalDate;
import java.util.ArrayList;

public class ArrayUtilTest {
    public static void main(String[] args) {
        Integer[] tab = {1, 2, 3, 4, 5};
        Integer[] tab2 = {5,3,7,2,9,11,1};
        LocalDate data1 = LocalDate.of(2000, 11, 1);
        LocalDate data2 = LocalDate.of(2001, 11, 1);
        LocalDate data3 = LocalDate.of(2002, 11, 1);
        LocalDate[] dates = {data1, data2, data3};
        LocalDate[] notSortedDates = {data2, data3, data1};
        //test for isSorted
        System.out.println("Czy elementy w tablicy Integer sa posortowane? " + ArrayUtil.isSorted(tab));
        System.out.println("Czy elementy w tablicy LocalDate sa posortowane? " + ArrayUtil.isSorted(dates));
        //test for binarySearch
        System.out.println("Index of 5 in tab - " + ArrayUtil.binSearch(tab, 5));
        System.out.println("Index of data2 in dates- " + ArrayUtil.binSearch(dates, data2));
        //test for selectionSort
        System.out.println("SelectionSort on Integer Array");
        ArrayUtil.selectionSort(tab2);
        System.out.println("SelectionSort on LocalDate Array (notSortedDates)");
        ArrayUtil.selectionSort(notSortedDates);
        //test for mergeSort
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(6);
        numbers.add(8);
        numbers.add(5);
        numbers.add(10);
        numbers.add(4);
        System.out.println("Integers before merge sort");
        for(int n: numbers) System.out.print(n + " ");
        System.out.println("\nIntegers after merge sort");
        ArrayUtil.mergeSort(numbers);
        for(int n: numbers) System.out.print(n + " ");
        ArrayList<LocalDate> dates2 = new ArrayList<>();
        dates2.add(LocalDate.of(2002, 12, 6));
        dates2.add(LocalDate.of(2001, 12, 6));
        dates2.add(LocalDate.of(2003, 12, 6));
        System.out.println("LocalDate before merge sort");
        for(LocalDate date: dates2) System.out.print(date + " ");
        System.out.println("\nLocalDate after merge sort");
        ArrayUtil.mergeSort(dates2);
        for(LocalDate date: dates2) System.out.print(date + " ");
    }
}
