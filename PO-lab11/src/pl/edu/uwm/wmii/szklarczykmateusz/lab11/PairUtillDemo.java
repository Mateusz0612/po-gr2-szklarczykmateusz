package pl.edu.uwm.wmii.szklarczykmateusz.lab11;

public class PairUtillDemo {
    public static void main(String[] args) {
        Pair<String> para = new Pair<>("Mateusz", "Jacek");
        Pair<String> zamienionaPara = PairUtil.swap(para);
        System.out.println(zamienionaPara.getFirst() + " " + zamienionaPara.getSecond());
    }
}
