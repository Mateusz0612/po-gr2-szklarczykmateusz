package pl.edu.uwm.wmii.szklarczykmateusz.lab11;


import java.util.ArrayList;

public class ArrayUtil<T extends Comparable<T>> {
    public static <T extends Comparable<? super T>> boolean isSorted(T[] a) {
        for (int i = 0; i < a.length; i++) {
            if (i + 1 == a.length) break;
            if (a[i].compareTo(a[i + 1]) > 0) return false;
        }
        return true;
    }

    public static <T extends Comparable<? super T>> int binSearch(T[] arr, T x) {
        int l = 0;
        int r = arr.length - 1;
        while (l <= r) {
            int m = l + (r - l) / 2;
            if (arr[m].compareTo(x) == 0)
                return m;
            if (arr[m].compareTo(x) < 0)
                l = m + 1;
            else
                r = m - 1;
        }
        return -1;
    }

    public static <T extends Comparable<? super T>> void selectionSort(T[] arr) {
        int n = arr.length;
        for (int i = 0; i < n; i++) {
            int minIndex = i;
            for (int j = i + 1; j < n; j++) {
                if (arr[j].compareTo(arr[minIndex]) < 0) minIndex = j;
            }
            T temp = arr[minIndex];
            arr[minIndex] = arr[i];
            arr[i] = temp;
            System.out.print("[");
            for (int k = 0; k < n; k++) {
                if (k == n - 1) System.out.print(arr[k]);
                else System.out.print(arr[k] + ",");
            }
            System.out.print("]\n");
        }
    }


    public static <T extends Comparable<? super T>> void mergeSort(ArrayList<T> tab) {
        if (tab.size() > 1) {
            ArrayList<T> left = new ArrayList<>();
            ArrayList<T> right = new ArrayList<>();
            boolean logicalSwitch = true;
            while (!tab.isEmpty()) {
                if (logicalSwitch) {
                    left.add(tab.remove(0));
                } else {
                    right.add(tab.remove(tab.size() / 2));
                }
                logicalSwitch = !logicalSwitch;
            }
            mergeSort(left);
            mergeSort(right);
            while (!left.isEmpty() && !right.isEmpty()) {
                if (left.get(0).compareTo(right.get(0)) <= 0) {
                    tab.add(left.remove(0));
                } else {
                    tab.add(right.remove(0));
                }
            }
            if (!left.isEmpty()) {
                tab.addAll(left);
            } else if (!right.isEmpty()) {
                tab.addAll(right);
            }
        }
    }
}
