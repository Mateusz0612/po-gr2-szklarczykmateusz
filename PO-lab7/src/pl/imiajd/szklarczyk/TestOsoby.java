package pl.imiajd.szklarczyk;

public class TestOsoby {

    public static void main(String[] args){
        Osoba Mati = new Osoba("Kowalski", 2000);
        Student student1 = new Student("Jackowski", 1995, "Matematyka");
        Nauczyciel matematyk = new Nauczyciel("Frankowski", 1976, 3000);
        System.out.println("Osoba");
        System.out.println(Mati);
        System.out.println("Nazwisko osoby " + Mati.getNazwisko());
        System.out.println("Student");
        System.out.println(student1);
        System.out.println("Kierunek studenta " + student1.getKierunek());
        System.out.println("Nauczyciel");
        System.out.println(matematyk);
        System.out.println("Pensja nauczyciela " + matematyk.getPensja());
    }
}
