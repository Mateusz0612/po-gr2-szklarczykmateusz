package pl.imiajd.szklarczyk;

public class Nauczyciel extends Osoba {

    public Nauczyciel(String nazwisko, int rokUrodzenia, int pensja) {
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    public int getPensja() {
        return pensja;
    }

    public String toString() {
        return super.toString() + " " + pensja;
    }

    private int pensja;
}
