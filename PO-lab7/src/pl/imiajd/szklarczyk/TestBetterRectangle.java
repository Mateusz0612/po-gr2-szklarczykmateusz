package pl.imiajd.szklarczyk;

public class TestBetterRectangle {
    public static void main(String[] args){
        BetterRectangle rect = new BetterRectangle(0, 0, 2, 5);
        int rectPerimeter = rect.getPerimeter();
        int rectArea = rect.getArea();
        System.out.println("Area " + rectArea);
        System.out.println("Preimeter " + rectPerimeter);
    }
}
