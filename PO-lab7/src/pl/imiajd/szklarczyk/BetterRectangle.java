package pl.imiajd.szklarczyk;

import java.awt.*;

public class BetterRectangle extends Rectangle {
    public BetterRectangle(int x, int y, int width, int height) {
        super(x, y, width, height);
    }

    public int getPerimeter(){
        return 2 * height + 2 * width;
    }

    public int getArea(){
        return height * width;
    }

}
