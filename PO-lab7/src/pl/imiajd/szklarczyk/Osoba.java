package pl.imiajd.szklarczyk;

public class Osoba {

    public Osoba(String nazwisko, int rokUrodzenia) {
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getRokUrodzenia() {
        return rokUrodzenia;
    }

    @Override
    public String toString() {
        return nazwisko + " " + rokUrodzenia;
    }

    private String nazwisko;
    private int rokUrodzenia;
}
