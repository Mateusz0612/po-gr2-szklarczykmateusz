package pl.imiajd.szklarczyk;

public class Adres {

    public Adres(String ulica, int number_domu, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.number_domu = number_domu;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public Adres(String ulica, int number_domu, int number_mieszkania, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.number_domu = number_domu;
        this.number_mieszkania = number_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public void pokaz() {
        System.out.println(this.kod_pocztowy + " " + this.miasto);
        if (this.number_mieszkania != 0) System.out.println(this.ulica + this.number_domu + this.number_mieszkania);
        else System.out.println(this.ulica + this.number_domu);
    }

    public boolean przed(String kod){
        int kodInt = Integer.parseInt(kod.substring(0, 2) + kod.substring(3, kod.length()));
        int thisKodInt = Integer.parseInt(this.kod_pocztowy.substring(0, 2) + this.kod_pocztowy.substring(2, this.kod_pocztowy.length()));
        return thisKodInt > kodInt;
    }

    private String ulica;
    private int number_domu;
    private int number_mieszkania = 0;
    private String miasto;
    private String kod_pocztowy;
}
