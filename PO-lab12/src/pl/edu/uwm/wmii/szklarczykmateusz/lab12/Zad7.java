package pl.edu.uwm.wmii.szklarczykmateusz.lab12;

import java.util.ArrayList;
import java.util.Scanner;

public class Zad7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj n");
        int n = scan.nextInt();
        ArrayList<Integer> pierwsze = new ArrayList<>();
        for (int i = 2; i <= n; i++) {
            pierwsze.add(i);
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            for(int j=2; j<pierwsze.size(); j++){
                if(pierwsze.get(j) % i == 0 && pierwsze.get(j) != i){
                    pierwsze.remove(pierwsze.get(j));
                }
            }
        }
        for(int element: pierwsze) System.out.println(element);
    }
}
