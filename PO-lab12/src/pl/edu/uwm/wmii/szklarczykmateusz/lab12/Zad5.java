package pl.edu.uwm.wmii.szklarczykmateusz.lab12;

import java.util.Scanner;
import java.util.Stack;

public class Zad5 {
    public static void main(String[] args) {
        Stack<String> stringStack = new Stack<>();
        Scanner scan = new Scanner(System.in);
        String word;
        do {
            System.out.println("Podaj slowo. Podanie slowa z kropka zakonczy podawanie slow");
            word = scan.next();
            stringStack.push(word);
        } while (!word.endsWith("."));
        while(!stringStack.isEmpty()){
            String element = stringStack.pop();
            if(element.endsWith(".")){
                String correctWord = element.substring(0, 1).toUpperCase() + element.substring(1, element.length() - 1);
                System.out.println(correctWord);
            } else if(stringStack.size() == 0){
                String correctWord = element.substring(0, 1).toLowerCase() + element.substring(1) + ".";
                System.out.println(correctWord);
            } else System.out.println(element);
        }
    }
}
