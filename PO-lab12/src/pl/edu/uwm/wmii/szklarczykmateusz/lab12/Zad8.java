package pl.edu.uwm.wmii.szklarczykmateusz.lab12;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;


public class Zad8 {
    public static void main(String[] args) {
        ArrayList<Integer> arr = new ArrayList<>();
        LinkedList<String> names = new LinkedList<>();
        names.add("Jhon");
        names.add("Frank");
        names.add("Einstein");
        for (int i = 0; i <= 10; i++) arr.add(i);
        System.out.println("For ArrayList of Integers");
        print(arr);
        System.out.println("For LinkedList of strings");
        print(names);
    }

    public static <E> void print(Iterable<E> element) {
        Iterator<E> iterator = element.iterator();
        while (iterator.hasNext()) {
            E item = iterator.next();
            if(!iterator.hasNext()) System.out.println(item);
            else System.out.print(item + ",");
        }
    }
}
