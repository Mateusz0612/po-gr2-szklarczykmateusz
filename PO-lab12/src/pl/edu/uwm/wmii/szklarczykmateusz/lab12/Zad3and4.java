package pl.edu.uwm.wmii.szklarczykmateusz.lab12;

import java.util.LinkedList;

public class Zad3and4 {
    public static void main(String[] args) {
        LinkedList<String> lista1 = new LinkedList<>();
        lista1.add("Mateusz");
        lista1.add("Jacek");
        lista1.add("Agnieszka");
        System.out.println("String LinkedList before reverse");
        for (String element : lista1) System.out.print(element + " ");
        odwroc(lista1);
        System.out.println("\nString LinkedList after reverse");
        for (String element : lista1) System.out.print(element + " ");
        LinkedList<Integer> lista2 = new LinkedList<>();
        lista2.add(1);
        lista2.add(2);
        lista2.add(3);
        System.out.println("\nInteger LinkedList before reverse");
        for (Integer element : lista2) System.out.print(element + " ");
        odwroc(lista2);
        System.out.println("\nInteger LinkedList after reverse");
        for (Integer element : lista2) System.out.print(element + " ");
    }

    public static <E> void odwroc(LinkedList<E> lista) {
        LinkedList<E> temporaryList = new LinkedList<>();
        for (int i = lista.size() - 1; i >= 0; i--)
            temporaryList.add(lista.get(i));
        for (int i = 0; i < lista.size(); i++) {
            lista.set(i, temporaryList.get(i));
        }
    }
}
