package pl.edu.uwm.wmii.szklarczykmateusz.lab12;

import java.util.Scanner;
import java.util.Stack;

public class Zad6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n;
        do{
            System.out.println("Podaj liczbe wieksza od 0 (ale nie rowna)");
            n = scan.nextInt();
        }while(n <=0);
        String nInString = Integer.toString(n);
        int nDigits = nInString.length();
        Stack<Integer> integerStack = new Stack<>();
        for(int i=0; i<nDigits; i++){
            int result = Integer.parseInt(nInString) % 10;
            integerStack.push(result);
            nInString = nInString.substring(0, nInString.length() - 1);
        }
        while(!integerStack.isEmpty()) {
            System.out.print(integerStack.pop() + " ");
        }
    }
}
