package pl.edu.uwm.wmii.szklarczykmateusz.lab12;


import java.util.Iterator;
import java.util.LinkedList;

public class Zad1and2 {

    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<>();
        pracownicy.add("Kowalski");
        pracownicy.add("Kaczmarczyk");
        pracownicy.add("Jackowski");
        pracownicy.add("Kolakowski");
        pracownicy.add("Leksykalski");
        pracownicy.add("Warowski");
        pracownicy.add("Ciekawski");
        System.out.println("Przed usunieciem");
        System.out.println("---TEST FOR STRING---");
        for (String p : pracownicy) System.out.print(p + " ");
        System.out.println("\nUsuwanie co 2 osoby");
        redukuj(pracownicy, 2);
        for (String p : pracownicy) System.out.print(p + " ");
        System.out.println("\n---TEST FOR INT---");
        LinkedList<Integer> numery = new LinkedList<>();
        for(int i=1; i<=7; i++) numery.add(i);
        System.out.println("Przed usunieciem");
        for(int n: numery) System.out.print(n + " ");
        redukuj(numery, 2);
        System.out.println("\nUsuwanie co 2 numerka");
        for(int n: numery) System.out.print(n + " ");
    }

    public static <T> void redukuj(LinkedList<T> pracownicy, int n) {
        Iterator<T> iterator = pracownicy.iterator();
        int toDelete = n - 1;
        int i = 0;
        while (iterator.hasNext()) {
            iterator.next();
            if (i == toDelete) {
                iterator.remove();
                toDelete += n;
            }
            i++;
        }
    }

}
