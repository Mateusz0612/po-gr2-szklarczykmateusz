package pl.edu.uwm.wmii.szklarczykmateusz.test2;

import java.util.ArrayList;

public class ObslugaKlienta {
    static double procentRabatu;
    private ArrayList<Klient> klienci;

    public ObslugaKlienta(ArrayList<Klient> klienci) {
        this.klienci = klienci;
    }

    public ArrayList<Klient> getKlienci(){
        return klienci;
    }

    public void setProcentRabatu(double value) {
        procentRabatu = value;
    }

    public static double discountAmount(Klient k) {
        if (k.getRachunek() >= 300.0) {
            return k.getRachunek() * ObslugaKlienta.procentRabatu;
        }
        return 0;
    }
}
