package pl.edu.uwm.wmii.szklarczykmateusz.test2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args) {
        //creating clients list
        ArrayList<Klient> klienci = new ArrayList<>();
        klienci.add(new Klient("Kowalski", 0, LocalDate.of(2010, 5, 12), 100.0));
        klienci.add(new Klient("Adamski", 1, LocalDate.of(2010, 5, 12), 120.0));
        klienci.add(new Klient("Wielki", 2, LocalDate.of(2011, 3, 13), 150.0));
        klienci.add(new Klient("Pawlowski", 3, LocalDate.of(2015, 2, 3), 150.0));
        klienci.add(new Klient("Warminski", 4, LocalDate.of(2010, 5, 12), 320.0));
        klienci.add(new Klient("Warminski", 5, LocalDate.of(2010, 5, 12), 315.0));
        klienci.add(new Klient("Szklarczyk", 6, LocalDate.of(2019, 9, 17), 3315.0));
        //creating Obslugaklienta object
        ObslugaKlienta grupa = new ObslugaKlienta(klienci);
        //setting procentRabatu to 0.05
        grupa.setProcentRabatu(0.05);
        //printing klients
        System.out.println("Przed sortowaniem");
        for (Klient element : grupa.getKlienci()) System.out.println(element);
        grupa.getKlienci().sort(Klient::compareTo);
        //printing sorted klients
        System.out.println("Po sortowaniu");
        for (Klient element : grupa.getKlienci()) System.out.println(element);
        //test for discountAmount method
        System.out.println("Rabat klienta " + klienci.get(4) + " wynosi " + ObslugaKlienta.discountAmount(klienci.get(4)));
        System.out.println("Rabat klienta " + klienci.get(2) + " wynosi " + ObslugaKlienta.discountAmount(klienci.get(2)));
        //getting DiscountMap
        TreeMap<Integer, TreeMap<String, Double>> discountMap = DiscountMap(klienci);
        //printing elements from DiscountMap
        for (Map.Entry<Integer, TreeMap<String, Double>> entry : discountMap.entrySet()) {
            Integer key = entry.getKey();
            TreeMap<String, Double> value = entry.getValue();
            System.out.print(key + "=> ");
            for (Map.Entry<String, Double> entry1 : value.entrySet()) {
                String nazwa = entry1.getKey();
                double kwotaRabatu = entry1.getValue();
                System.out.print("Nazwa: " + nazwa + " KwotaRabatu:" + kwotaRabatu);
            }
            System.out.println();
        }
    }

    public static TreeMap<Integer, TreeMap<String, Double>> DiscountMap(ArrayList<Klient> klist) {
        TreeMap<Integer, TreeMap<String, Double>> discountMap = new TreeMap<>();
        for (Klient element : klist) {
            if (ObslugaKlienta.discountAmount(element) > 0) {
                TreeMap<String, Double> values = new TreeMap<>();
                values.put(element.getNazwa(), ObslugaKlienta.discountAmount(element));
                discountMap.put(element.getId(), values);
            }
        }
        return discountMap;
    }
}
