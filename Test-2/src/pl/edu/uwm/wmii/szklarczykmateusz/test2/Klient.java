package pl.edu.uwm.wmii.szklarczykmateusz.test2;

import java.time.LocalDate;

public class Klient implements Cloneable, Comparable<Klient> {

    private String nazwa;
    private int id;
    private LocalDate dataZakupy;
    private double rachunek;

    public Klient(String nazwa, int id, LocalDate dataZakupy, double rachunek) {
        this.nazwa = nazwa;
        this.id = id;
        this.dataZakupy = dataZakupy;
        this.rachunek = rachunek;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getId() {
        return id;
    }

    public LocalDate getDataZakupy() {
        return dataZakupy;
    }

    public double getRachunek() {
        return rachunek;
    }


    @Override
    public int compareTo(Klient other) {
        int result = dataZakupy.compareTo(other.dataZakupy);
        if (result == 0) {
            int result2 = nazwa.compareTo(other.nazwa);
            if (result2 == 0) {
                return Double.compare(rachunek, other.rachunek);
            }
            return result2;
        }
        return result;
    }

    @Override
    public String toString(){
        return "Klient[" + nazwa + "," + id + "," + dataZakupy + "," + rachunek + "]";
    }

}
