package pl.imiajd.szklarczyk;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrument {
    public static void main(String[] args){
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        LocalDate dataFlet1 = LocalDate.of(2019, 5, 3);
        LocalDate dataFortepian1 = LocalDate.of(2018, 3, 12);
        LocalDate dataSkrzypce1 = LocalDate.of(2017, 2, 2);
        Flet flet1 = new Flet("ProducentFlet", dataFlet1);
        Flet flet2 = new Flet("ProducentFlet", dataFlet1);
        Fortepian fortepian1 = new Fortepian("ProducentFortepianow", dataFortepian1);
        Fortepian fortepian2 = new Fortepian("ProducentInnychFortepianow", dataFortepian1);
        Skrzypce skrzypce1 = new Skrzypce("ProducentSkrzypcow", dataSkrzypce1);
        orkiestra.add(flet1);
        orkiestra.add(flet2);
        orkiestra.add(fortepian1);
        orkiestra.add(fortepian2);
        orkiestra.add(skrzypce1);
        for( Instrument p : orkiestra){
            p.dzwiek();
        }
        for( Instrument p : orkiestra){
            System.out.println(p);
        }
    }
}
