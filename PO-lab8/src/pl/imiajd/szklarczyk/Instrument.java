package pl.imiajd.szklarczyk;

import java.time.LocalDate;

abstract class Instrument {

    public Instrument(String producent, LocalDate rokProdukcji) {
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public String getProducent() {
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    abstract void dzwiek();

    public boolean equals(Object otherObject) {
        if (this == otherObject) return true;
        if (otherObject == null) return false;
        if (getClass() != otherObject.getClass()) return false;
        Instrument other = (Instrument) otherObject;
        if (rokProdukcji.equals(other.rokProdukcji) && producent.equals(other.producent)) return true;
        return false;
    }

    public String toString() {
        return "Producent: " + producent + " rok produkcji: " + rokProdukcji;
    }

    private LocalDate rokProdukcji;
    private String producent;
}
