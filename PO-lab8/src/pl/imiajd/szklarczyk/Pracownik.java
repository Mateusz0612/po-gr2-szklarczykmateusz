package pl.imiajd.szklarczyk;


import java.time.LocalDate;

class Pracownik extends Osoba {
    public Pracownik(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory() {
        return pobory;
    }

    @Override
    public String getOpis() {
        StringBuilder returnString = new StringBuilder("Imiona :");
        for (String p : getImiona()) {
            returnString.append(" ").append(p).append(" ");
        }
        returnString.append(" Nazwisko: ").append(getNazwisko());
        returnString.append(" Data urodzenia: ").append(getDataUrodzenia());
        returnString.append(" Plec: ").append(getPlec());
        returnString.append(" Pobory: ").append(getPobory());
        returnString.append(" Data zatrudnienia: ").append(getDataZatrudnienia());
        return returnString.toString();
    }

    public LocalDate getDataZatrudnienia() {
        return LocalDate.of(dataZatrudnienia.getYear(), dataZatrudnienia.getMonthValue(), dataZatrudnienia.getDayOfMonth());
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}