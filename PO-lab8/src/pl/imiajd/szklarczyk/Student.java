package pl.imiajd.szklarczyk;

import java.time.LocalDate;

class Student extends Osoba {
    public Student(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public String getOpis() {
        StringBuilder returnString = new StringBuilder("Imiona :");
        for (String p : getImiona()) {
            returnString.append(" ").append(p).append(" ");
        }
        returnString.append(" Nazwisko: ").append(getNazwisko());
        returnString.append(" Data urodzenia: ").append(getDataUrodzenia());
        returnString.append(" Plec: ").append(getPlec());
        returnString.append(" Kierunek: ").append(getKierunek());
        returnString.append(" Srednia ocen: ").append(getSredniaOcen());
        return returnString.toString();
    }

    public double getSredniaOcen() {
        return sredniaOcen;
    }

    public void setSredniaOcen(double value) {
        sredniaOcen = value;
    }

    public String getKierunek() {
        return kierunek;
    }

    private String kierunek;
    private double sredniaOcen;
}
