package pl.imiajd.szklarczyk;

import java.time.LocalDate;

public class TestOsoba {
    public static void main(String[] args) {
        Osoba[] ludzie = new Osoba[2];

        String[] namesJacek = {"Jacek", "Jan"};
        String[] namesMalg = {"Malgorzata", "Katarzyna"};
        LocalDate dataUrKowalski = LocalDate.of(2000, 12, 5);
        LocalDate dataZatrKowalski = LocalDate.of(2019, 5, 2);
        LocalDate dataUrMalg = LocalDate.of(1999, 11, 4);
        ludzie[0] = new Pracownik("Kowalski", namesJacek, dataUrKowalski, false, 3000.0, dataZatrKowalski);
        ludzie[1] = new Student("Nowak", namesMalg, dataUrMalg, true, "Matematyka", 3.0);

        for (Osoba p : ludzie) {
            System.out.println(p.getOpis());
        }
    }
}
