package pl.imiajd.szklarczyk;

import java.time.LocalDate;

abstract class Osoba {
    public Osoba(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec) {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko() {
        return nazwisko;
    }

    public String[] getImiona() {
        return imiona.clone();
    }

    public LocalDate getDataUrodzenia() {
        return LocalDate.of(dataUrodzenia.getYear(), dataUrodzenia.getMonthValue(), dataUrodzenia.getDayOfMonth());
    }

    public String getPlec() {
        if (plec) return "Kobieta";
        return "Mezczyzna";
    }

    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;
}
