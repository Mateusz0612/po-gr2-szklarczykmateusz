package pl.edu.uwm.wmii.szklarczykmateusz.lab8;

import java.util.*;
import java.time.LocalDate;

public class TestOsoba {
    public static void main(String[] args) {
        Osoba[] ludzie = new Osoba[2];

        String[] namesJacek = {"Jacek", "Jan"};
        String[] namesMalg = {"Malgorzata", "Katarzyna"};
        LocalDate dataUrKowalski = LocalDate.of(2000, 12, 5);
        LocalDate dataZatrKowalski = LocalDate.of(2019, 5, 2);
        LocalDate dataUrMalg = LocalDate.of(1999, 11, 4);
        ludzie[0] = new Pracownik("Kowalski", namesJacek, dataUrKowalski, false, 3000.0, dataZatrKowalski);
        ludzie[1] = new Student("Nowak", namesMalg, dataUrMalg, true, "Matematyka", 3.0);

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
    }
}

abstract class Osoba {
    public Osoba(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec) {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko() {
        return nazwisko;
    }

    public String[] getImiona() {
        return imiona.clone();
    }

    public LocalDate getDataUrodzenia() {
        return LocalDate.of(dataUrodzenia.getYear(), dataUrodzenia.getMonthValue(), dataUrodzenia.getDayOfMonth());
    }

    public String getPlec() {
        if (plec) return "Kobieta";
        return "Mezczyzna";
    }

    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;
}

class Pracownik extends Osoba {
    public Pracownik(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory() {
        return pobory;
    }

    public String getOpis() {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }

    public LocalDate getDataZatrudnienia(){
        return LocalDate.of(dataZatrudnienia.getYear(), dataZatrudnienia.getMonthValue(), dataZatrudnienia.getDayOfMonth());
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}


class Student extends Osoba {
    public Student(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec,String kierunek, double sredniaOcen) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis() {
        return "kierunek studiów: " + kierunek;
    }

    public double getSredniaOcen(){
        return sredniaOcen;
    }

    public void setSredniaOcen(double value){
        sredniaOcen = value;
    }

    private String kierunek;
    private double sredniaOcen;
}