package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class Game {

    public static List<Card> setValue() {
        List<Card> Cards = new ArrayList<>();
        String[] types = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "D", "K", "AS"};
        int typesIndex = 0;
        for (int i = 2; i < 14; i++) {
            Card card = new Card(types[typesIndex], i);
            typesIndex++;
            Cards.add(card);
        }
        return Cards;
    }

    public static String chooseWinner(Player p1, Player p2) {
        if (p1.playerCard.value > p2.playerCard.value) return "Wygral " + p1.playerName;
        else if (p2.playerCard.value > p1.playerCard.value) return "Wygral " + p2.playerName;
        return "Remis";
    }

    public static void init() {
        List<Card> gameCards;
        gameCards = Game.setValue();
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj nazwe pierwszego gracza");
        String playerOneName = scan.next();
        System.out.println("Podaj nazwe drugiego gracza");
        String playerTwoName = scan.next();
        Player playerOne = new Player(Card.randomCard(gameCards), playerOneName);
        Player playerTwo = new Player(Card.randomCard(gameCards), playerTwoName);
        String winnerName = Game.chooseWinner(playerOne, playerTwo);
        System.out.println(playerOne.playerName);
        playerOne.playerCard.drawCardInfo();
        System.out.println(playerTwo.playerName);
        playerTwo.playerCard.drawCardInfo();
        System.out.println("Wynik gry: " + winnerName);
    }
}
