package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        double num1 = 1.0;
        double num2 = 0.0;
        int result = Double.compare(num1, num2);
        System.out.println(result);
    }

    public static int getInput() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe naturalna:");
        return scan.nextInt();
    }
}
