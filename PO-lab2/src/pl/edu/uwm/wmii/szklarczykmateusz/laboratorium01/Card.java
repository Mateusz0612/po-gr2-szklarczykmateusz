package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;


import java.util.List;

public class Card {
    String type;
    int value;

    public Card(String type, int value) {
        this.type = type;
        this.value = value;
    }

    public void drawCardInfo() {
        System.out.println("Karta " + this.type);
    }

    public static Card randomCard(List<Card> list){
        int index = (int)(Math.random() * 12);
        return list.get(index);
    }
}
