package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.*;

public class Zad2 {

    public static void zad1a() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        int counter = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe:");
            int x = scan.nextInt();
            if (x % 2 == 0) counter++;
        }
        System.out.println("Podales/as " + n + " liczby z czego " + counter + " sa parzyste");
    }

    public static void zad1b() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        int counter = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe:");
            int x = scan.nextInt();
            if (x % 3 == 0 && x % 5 != 0) counter++;
        }
        System.out.println("Podales/as " + n + " liczby z czego " + counter + " sa podzielne przez 3 i niepodzielne przez 5");
    }

    public static void zad1c() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        int counter = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe:");
            int x = scan.nextInt();
            if (Math.sqrt(x) % 2 == 0) {
                counter++;
            }
        }
        System.out.println("Podales/as " + n + " liczby z czego " + counter + " sa kwadratami liczby parzystej");
    }

    public static void zad1f() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        int counter = 0;
        for (int i = 1; i <= n; i++) {
            System.out.println("Podaj liczbe:");
            int x = scan.nextInt();
            if (i % 2 != 1 && x % 2 == 0) counter++;
        }
        System.out.println("Podales/as " + n + " liczby z czego " + counter + " maja nieparzysty numer i sa parzyste");
    }

    public static void zad1g() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        int counter = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe:");
            int x = scan.nextInt();
            if (x % 2 != 0 && x >= 0) counter++;
        }
        System.out.println("Podales/as " + n + " liczby z czego " + counter + " sa nieparzyste i nieujemne");
    }

    public static void zad2() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        double result = 0.0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe:");
            double x = scan.nextDouble();
            if (x > 0.0) {
                result += 2 * x;
            }
        }
        System.out.println("Wynik: " + result);
    }

    public static void zad3() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        int positiveCounter = 0;
        int negativeCounter = 0;
        int zeros = 0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe:");
            double x = scan.nextDouble();
            if (x > 0.0) positiveCounter++;
            else if (x < 0.0) negativeCounter++;
            else if (x == 0.0) zeros++;
        }
        System.out.println("Dodatnich: " + positiveCounter + " Ujemnych: " + negativeCounter + " Zer: " + zeros);
    }

    public static void zad4() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        double max = 0.0;
        double min = 0.0;
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe rzeczywista");
            double x = scan.nextDouble();
            if (i == 0) {
                max = x;
                min = x;
            }
            if (x > max) max = x;
            if (x < min) min = x;

        }
        System.out.println("Najwieksza: " + max + " Najmniejsza: " + min);
    }

    public static void zad5() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        double[] arr = new double[n];
        for (int i = 0; i < n; i++) {
            System.out.println("Podaj liczbe:");
            double x = scan.nextDouble();
            arr[i] = x;
        }
        for (int i = 0; i < n - 1; i++) {
            if (arr[i] > 0 && arr[i + 1] > 0) {
                System.out.println("(" + arr[i] + "," + arr[i + 1] + ")");
            }
        }
    }
}
