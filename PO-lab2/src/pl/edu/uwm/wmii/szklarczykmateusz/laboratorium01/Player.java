package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

public class Player {
     Card playerCard;
     String playerName;

     public Player(Card playerCard, String playerName){
         this.playerCard = playerCard;
         this.playerName = playerName;
     }
}
