package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad1 {
    public static void a() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        double result = 0.0;
        for (int i = 1; i <= n; i++) {
            System.out.println("Podaj liczbe rzeczywista");
            double x = scan.nextDouble();
            result += x;
        }
        System.out.println(result);
    }

    public static void b() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        double result = 1.0;
        for (int i = 1; i <= n; i++) {
            System.out.println("Podaj liczbe rzeczywista");
            double x = scan.nextDouble();
            result *= x;
        }
        System.out.println(result);
    }

    public static void c() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        double result = 0.0;
        for (int i = 1; i <= n; i++) {
            System.out.println("Podaj liczbe rzeczywista");
            double x = scan.nextDouble();
            result += Math.abs(x);
        }
        System.out.println(result);
    }

    public static void d() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        double result = 0.0;
        for (int i = 1; i <= n; i++) {
            System.out.println("Podaj liczbe rzeczywista");
            double x = scan.nextDouble();
            result += Math.sqrt(Math.abs(x));
        }
        System.out.println(result);
    }

    public static void e() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        double result = 1;
        for (int i = 1; i <= n; i++) {
            System.out.println("Podaj liczbe rzeczywista");
            double x = scan.nextDouble();
            result *= Math.abs(x);
        }
        System.out.println(result);
    }

    public static void f() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        double result = 0;
        for (int i = 1; i <= n; i++) {
            System.out.println("Podaj liczbe rzeczywista");
            double x = scan.nextDouble();
            result += Math.pow(x, 2);
        }
        System.out.println(result);
    }

    public static void g() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        double resultAdd = 0;
        double resultMultiply = 1;
        for (int i = 1; i <= n; i++) {
            System.out.println("Podaj liczbe rzeczywista");
            double x = scan.nextDouble();
            resultAdd += x;
            resultMultiply *= x;
        }
        System.out.println("Add " + resultAdd + " Multiply " + resultMultiply);
    }

    public static void h() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        double result = 0;
        for (int i = 1; i <= n; i++) {
            System.out.println("Podaj liczbe rzeczywista");
            double x = scan.nextDouble();
            if (i % 2 == 0) result -= x;
            else result += x;
        }
        System.out.println("Wynik: " + result);
    }

    public static int factorial(int x) {
        int result = 1;
        for (int i = 1; i <= x; i++) result *= i;
        return result;
    }

    public static void i() {
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        double result = 0.0;
        for (int i = 1; i <= n; i++) {
            System.out.println("Podaj liczbe rzeczywista");
            double x = scan.nextDouble();
            if (i % 2 != 0) {
                result -= (x) / (factorial(i));
            } else {
                result += (x) / (factorial(i));
            }
        }
        System.out.println(result);
    }

    public static void one_two(){
        Scanner scan = new Scanner(System.in);
        int n = Main.getInput();
        double[] list = new double[n];
        for(int i=0; i<n; i++){
            System.out.println("Podaj liczbe rzeczywista");
            double x = scan.nextDouble();
            list[i] = x;
        }
        System.out.println("Wyrazy ciagu:");
        for(int i=n-1; i>=0; i--){
            System.out.printf("%.2f ", list[i]);
        }
    }

}
