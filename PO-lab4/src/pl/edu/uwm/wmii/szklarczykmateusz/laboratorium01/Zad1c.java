package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad1c {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj wyraz");
        String word = scan.next();
        String x = middle(word);
        System.out.println(x);
    }

    public static String middle(String str){
        int strLen = str.length();
        if(strLen % 2 != 0){
            int index = strLen/2;
            return str.substring(index , index + 1);
        }
        else{
            int index = strLen/2;
            int index2 = (strLen/2) - 1;
            return str.substring(index2, index + 1);
        }
    }
}
