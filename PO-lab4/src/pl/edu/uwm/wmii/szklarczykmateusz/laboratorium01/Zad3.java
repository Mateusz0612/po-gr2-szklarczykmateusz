package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zad3 {
    public static void main(String[] args){
        zliczWyrazy("test.txt", "jacek");
    }

    public static void zliczWyrazy(String fileName, String word){
        try{
            File file = new File(fileName);
            Scanner scan = new Scanner(file);
            int counter = 0;
            while(scan.hasNextLine()){
                String data = scan.nextLine();
                counter += Zad1b.countSubStr(data, word);
            }
            scan.close();
            System.out.println(counter);
        } catch (FileNotFoundException e){
            System.out.println("Nie znaleziono takiego pliku");
            e.printStackTrace();
        }
    }
}
