package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad1g {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj cyfry");
        String word = scan.next();
        String changedWord = nice(word);
        System.out.println(changedWord);
    }

    public static String nice(String str) {
        StringBuffer reversedWord = new StringBuffer();
        int counter = 0;
        for (int i = str.length() - 1; i >= 0; i--) {
            if(counter == 3){
                reversedWord.append('\'');
                counter = 0;
            }
            reversedWord.append(str.charAt(i));
            counter++;
        }
        return reversedWord.reverse().toString();
    }
}
