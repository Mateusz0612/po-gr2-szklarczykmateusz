package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad1h {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj cyferki");
        String word = scan.next();
        System.out.println("Podaj cyfre");
        int x = scan.nextInt();
        System.out.println("Podaj separator");
        char sep = scan.next().charAt(0);
        String changedWord = nice(word, x, sep);
        System.out.println(changedWord);
    }

    public static String nice(String str, int n, char c){
        StringBuffer reversedWord = new StringBuffer();
        int counter = 0;
        for (int i = str.length() - 1; i >= 0; i--) {
            if(counter == n){
                reversedWord.append(c);
                counter = 0;
            }
            reversedWord.append(str.charAt(i));
            counter++;
        }
        return reversedWord.reverse().toString();
    }
}
