package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad1d {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj slowo");
        String word = scan.next();
        System.out.println("Ile razy powtorzyc");
        int y = scan.nextInt();
        String x = repeat(word, y);
        System.out.println(x);
    }

    public static String repeat(String str, int n){
        String newStr = "";
        for(int i=0; i<n; i++){
            newStr = newStr.concat(str);
        }
        return newStr;
    }
}
