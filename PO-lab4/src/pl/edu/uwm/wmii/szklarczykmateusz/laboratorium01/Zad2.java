package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zad2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("podaj literke");
        char c = scan.next().charAt(0);
        zlicz("test.txt", c);
    }

    public static void zlicz(String fileName, char c) {
        try {
            int counter = 0;
            File file = new File(fileName);
            Scanner scan = new Scanner(file);
            while (scan.hasNextLine()) {
                String data = scan.nextLine();
                if(data.contains(Character.toString(c))) counter++;
            }
            System.out.println(counter);
            scan.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
            e.printStackTrace();
        }
    }
}
