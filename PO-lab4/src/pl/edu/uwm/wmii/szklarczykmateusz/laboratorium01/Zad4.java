package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.math.BigInteger;
import java.util.Scanner;

public class Zad4 {

    public static void main(String[] args) {
        int n = Integer.valueOf(args[0]);
        BigInteger sumka = new BigInteger("0");
        int wynik;
        for (int i = 1; i <= n; i++) {
            if (i == 1) wynik = 1;
            else if (i == 2) wynik = 2;
            else wynik = 2 * (i - 1);
            BigInteger toAdd = new BigInteger(String.valueOf(wynik));
            sumka = sumka.add(toAdd);
        }
        System.out.println(sumka.toString());
    }
}
