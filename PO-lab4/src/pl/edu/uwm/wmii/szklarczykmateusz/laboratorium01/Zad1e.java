package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad1e {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj pierwszy wyraz");
        String word1 = scan.next();
        System.out.println("Podaj drugi wyraz");
        String word2 = scan.next();
        int[] arr = where(word1, word2);
        for (int e : arr) System.out.println(e);
    }

    public static int[] where(String str, String subStr) {
        int lastIndex = 0;
        int k = 0;
        int[] indexArr = new int[str.length()];
        while(lastIndex != -1){
            lastIndex = str.indexOf(subStr,lastIndex);
            indexArr[k] = str.indexOf(subStr, lastIndex);
            k++;
            if(lastIndex != -1){
                lastIndex += subStr.length();
            }
        }
        return indexArr;
    }
}
