package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad1f {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj wyraz");
        String word = scan.next();
        String changedWord = change(word);
        System.out.println(changedWord);
    }

    public static String change(String str){
        StringBuffer word = new StringBuffer();
        for(int i=0; i<str.length(); i++){
            if(Character.isLowerCase(str.charAt(i))) word.append(Character.toUpperCase(str.charAt(i)));
            else word.append(Character.toLowerCase(str.charAt(i)));
        }
        return word.toString();
    }
}
