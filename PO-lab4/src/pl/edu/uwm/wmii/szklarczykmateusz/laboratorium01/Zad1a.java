package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.lang.reflect.Array;
import java.util.Scanner;

public class Zad1a {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj wyraz");
        String str = scan.next();
        System.out.println("Podaj literke");
        char c = scan.next().charAt(0);
        int x = countChar(str, c);
        System.out.println("Wynik: " + x);
    }

    public static int countChar(String str, char c) {
        char[] letters = new char[str.length()];
        for (int i = 0; i < str.length(); i++) letters[i] = str.charAt(i);
        int counter = 0;
        for(char letter: letters){
            if(letter == c) counter++;
        }
        return counter;
    }
}
