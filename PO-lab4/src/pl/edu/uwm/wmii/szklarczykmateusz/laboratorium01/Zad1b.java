package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.util.Scanner;

public class Zad1b {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj pierwszy napis");
        String str = scan.next();
        System.out.println("Podaj drugi napis");
        String subStr = scan.next();
        int x = countSubStr(str, subStr);
        System.out.println("Wynik: " + x);
    }

    public static int countSubStr(String str, String subStr) {
        int lastIndex = 0;
        int count = 0;
        while(lastIndex != -1){
            lastIndex = str.indexOf(subStr,lastIndex);
            if(lastIndex != -1){
                count ++;
                lastIndex += subStr.length();
            }
        }
        return count;
    }
}
