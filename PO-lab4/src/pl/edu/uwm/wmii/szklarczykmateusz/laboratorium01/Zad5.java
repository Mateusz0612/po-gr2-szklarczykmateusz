package pl.edu.uwm.wmii.szklarczykmateusz.laboratorium01;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Zad5 {
    public static void main(String[] args){
        int n = Integer.parseInt(args[0]);
        BigDecimal p = BigDecimal.valueOf(Double.parseDouble(args[1]));
        BigDecimal k = BigDecimal.valueOf(Double.parseDouble(args[2]));
        for(int i=1; i<= n; i++){
            k = k.multiply(p.divide(BigDecimal.valueOf(100)));
        }
        k = k.setScale(2, RoundingMode.CEILING);
        System.out.println(k.toString());
    }
}
